package br.com.alura.leilao.ui.activity;

import android.content.Context;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.alura.leilao.api.retrofit.client.LeilaoWebClient;
import br.com.alura.leilao.api.retrofit.client.RespostaListener;
import br.com.alura.leilao.model.Leilao;
import br.com.alura.leilao.ui.recyclerview.adapter.ListaLeilaoAdapter;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class ListaLeilaoActivityTest {

    @Mock
    private Context context;

    @Spy
    private ListaLeilaoAdapter listaLeilaoAdapter = new ListaLeilaoAdapter(context);

    @Mock
    public LeilaoWebClient client;

    @Test
    public void deve_AtualizarListaDeLeiloes_QuandoBuscarLeiloesDaApi() {
        ListaLeilaoActivity listaLeilaoActivity = new ListaLeilaoActivity();
        Mockito.doNothing().when(listaLeilaoAdapter).atualizaLista();

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                RespostaListener<List<Leilao>> argument = invocation.getArgument(0);//referente ao -> new RespostaListener<List<Leilao>>()
                argument.sucesso(new ArrayList<>(Arrays.asList(
                        new Leilao("Carro"),
                        new Leilao("Computador"),
                        new Leilao("MAC")
                )));
                return null;
            }
        }).when(client).todos(ArgumentMatchers.any(RespostaListener.class));

        listaLeilaoActivity.buscaLeiloes(listaLeilaoAdapter, client);

        int quantidadeDeItensNalista = listaLeilaoAdapter.getItemCount();
        assertThat(quantidadeDeItensNalista, CoreMatchers.is(3));
    }
}